export const API_BASE_URI = process.env.REACT_APP_API_BASE_URI;
export const ACCESS_TOKEN = process.env.REACT_APP_ACCESS_TOKEN;