import { lazy } from 'react';
import { Routes, Route } from 'react-router-dom';
import * as path from './path';

const Home = lazy(() => import('../../components/pages/Home'));
const Repos = lazy(() => import('../../components/pages/Repos'));
const Issues = lazy(() => import('../../components/pages/Issues'));

function Router() {
    return(
        <Routes>
            <Route exact path={path.HOME} element={<Home />} />
            <Route exact path={`${path.REPOS}/:slug`} element={<Repos />} />
            <Route exact path={`${path.ISSUES}/:slug`} element={<Issues />} />
        </Routes>
    )
}

export default Router;