import {useState, useEffect} from 'react'
import { Link } from 'react-router-dom'

import * as path from '../../../services/Router/path';
import { API_BASE_URI } from '../../../constants/url';
import { ACCESS_TOKEN } from '../../../constants/url';
import Pagination from '../../molecules/Pagination'

function Index() {
    const [reposLoading, setReposLoading] = useState(false);
    const [repos, setRepos] = useState(null);
    const [pageInfo, setPageInfo] = useState(null);
    const [after, setAfter] = useState(null);
    const [before, setBefore] = useState(null);
    const userName = localStorage.getItem('user')

    const LAUNCHES_QUERY =`
        query {
            user(login: ${userName}) {
                repositories(first: 10, after: ${after}, before: ${before}){
                    nodes{
                        id
                        name
                        stargazerCount
                        watchers{
                            totalCount
                        }   
                    }
                    pageInfo {
                        startCursor
                        endCursor
                        hasNextPage
                        hasPreviousPage
                    }
                }
            }
        }
    `

    const fetching = () => {
        setReposLoading(true)
        fetch(API_BASE_URI, {
            // mode: "no-cors",
            method: "POST",
            headers: { "Content-Type": "application/json", Authorization: "bearer "+ACCESS_TOKEN },
            body: JSON.stringify({ query: LAUNCHES_QUERY })
        }).then(response => response.json())
        .then(data => {
            setRepos(data.data.user.repositories.nodes) 
            setPageInfo(data.data.user.repositories.pageInfo) 
            setReposLoading(false)
        } )
        .catch(error => {
            console.log(error) 
            setReposLoading(false)
        })
    }

    const handleAfter = (value) => {
        setAfter(value)
    }
    const handleBefore = (value) => {
        setBefore(value)
    }

    useEffect(() => {
        fetching()
    }, [after])

    return (
        <div className="w-4/5 mx-auto pt-10">
            <h1 className="text-2xl">Repositories</h1>
            <div className="mt-1">
                <Link to={"/"} className="text-blue-400">Home </Link>
                <span className="text-gray-500">/ Repositories</span>
            </div>
            {reposLoading ? (
                <div className="text-center mt-40"><p>Loading...</p></div>
            ): repos?.length > 0 ? (
                <div className="mt-10">
                    {
                        repos.map((repo, index) => (
                            <Link to={`${path.ISSUES}/${repo.name}`} key={index}>
                                <div className="flex justify-between mb-4">
                                    <h3 className="font-semibold">{repo.name}</h3>
                                    <p>{repo.stargazerCount} stars / {repo.watchers.totalCount} watching </p>
                                </div>
                            </Link>
                        ))
                    }
                    {
                        pageInfo && (
                            <Pagination 
                                hasPreviousPage={pageInfo.hasPreviousPage}
                                hasNextPage={pageInfo.hasNextPage}
                                handleAfter={handleAfter}
                                handleBefore={handleBefore}
                                startCursor={pageInfo.startCursor}
                                endCursor={pageInfo.endCursor}
                            />
                        )
                    }
                </div>
            ): <div className="text-center mt-20"><h1 className="text-lg">No repositories found</h1></div>}
        </div>
    )
}

export default Index
