import { useState } from 'react';
import { Link } from 'react-router-dom'

import * as path from '../../../services/Router/path';
import { API_BASE_URI } from '../../../constants/url';
import { ACCESS_TOKEN } from '../../../constants/url';

function Index() {
    const [userInput, setUserInput] = useState("");
    const [userLoading, setUserLoading] = useState(false);
    const [error, setError] = useState(null);
    const [users, setUsers] = useState(null);
    const [slug, setSlug] = useState(null);

    const onChange = (e) => {
        setSlug(e.target.value)
        setUserInput(JSON.stringify(e.target.value))
    }

    const LAUNCHES_QUERY =`
        query {
            user(login: ${userInput}) {
                login
                name
                avatarUrl
            }
        }
    `

    const handleSubmit = (e) => {
        localStorage.setItem('user', userInput);
        setUsers(null)
        setError(null)
        setUserLoading(true)
        e.preventDefault();
        fetch(API_BASE_URI, {
            // mode: "no-cors",
            method: "POST",
            headers: { "Content-Type": "application/json", Authorization: "bearer "+ACCESS_TOKEN },
            body: JSON.stringify({ query: LAUNCHES_QUERY })
        }).then(response => response.json())
        .then(data => {
            setUsers(data.data.user) 
            {data.errors && setError(data.errors[0].message)}
            setUserLoading(false)
        } )
        .catch(error => {
            console.log(error) 
            setUserLoading(false)
        })
    }

    return (
        <>  
            <div className="w-4/5 mx-auto pt-20">
                <form onSubmit={handleSubmit} className="w-full flex justify-center">
                    <input 
                        className="outline-none border border-black px-2.5 py-2 w-80" 
                        type="text" 
                        placeholder="Search users..." 
                        onChange={ e => onChange(e)}
                    />
                    <button className={`bg-black text-white px-6 ml-2 ${userLoading ? 'opacity-70 cursor-not-allowed': ''}`} disabled={userLoading}>Search</button>
                </form>
                {userLoading && (
                    <div className="text-center mt-40"><p>Loading...</p></div>
                )}
                {error && <div className="text-center mt-20"><h1 className="text-lg">{error}</h1></div> }
                {users && (
                    <>
                        <Link to={`${path.REPOS}/${slug}`}>
                            <div className="mt-20 flex justify-center">
                                <div className="text-center">
                                    <img className="h-40 w-40 rounded-2xl" src={users.avatarUrl} alt="avatar" />
                                    <h3 className="mt-2 text-xl">{users.name}</h3>
                                </div>
                            </div>
                        </Link>
                    </> 
                )}
            </div>  
        </>
    );
}

export default Index;
