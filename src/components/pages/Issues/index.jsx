import {useState, useEffect} from 'react'
import { useParams } from 'react-router-dom';
import { Link } from 'react-router-dom'

import * as path from '../../../services/Router/path';
import { API_BASE_URI } from '../../../constants/url';
import { ACCESS_TOKEN } from '../../../constants/url';
import Pagination from '../../molecules/Pagination'
import NewIssueModal from '../../molecules/NewIssueModal'

function Index() {
    const { slug } = useParams();
    const [issuesLoading, setIssuesLoading] = useState(false);
    const [createSuccess, setCreateSuccess] = useState(null);
    const [issues, setIssues] = useState(null);
    const [repoId, setRepoId] = useState(null);
    const [pageInfo, setPageInfo] = useState(null);
    const [after, setAfter] = useState(null);
    const [before, setBefore] = useState(null);
    const repoName = JSON.stringify(slug)
    const userName = localStorage.getItem('user')

    const LAUNCHES_QUERY =`
        query {
            repository(owner: ${userName} name:${repoName}){
                id
                issues(first: 10, after: ${after}, before: ${before}){
                    edges{
                        node{
                            createdAt
                            title
                            author{
                                login
                            }
                        }
                    }
                    pageInfo {
                        startCursor
                        endCursor
                        hasNextPage
                        hasPreviousPage
                    }
                }
            }
        }
    `

    const fetching = () => {
        setIssuesLoading(true)
        fetch(API_BASE_URI, {
            // mode: "no-cors",
            method: "POST",
            headers: { "Content-Type": "application/json", Authorization: "bearer "+ACCESS_TOKEN },
            body: JSON.stringify({ query: LAUNCHES_QUERY })
        }).then(response => response.json())
        .then(data => {
            setIssues(data.data.repository.issues.edges) 
            setPageInfo(data.data.repository.issues.pageInfo) 
            setRepoId(data.data.repository.id)
            setIssuesLoading(false)
        } )
        .catch(error => {
            console.log(error) 
            setIssuesLoading(false)
        })
    }

    const handleAfter = (value) => {
        setAfter(value)
    }
    const handleBefore = (value) => {
        setBefore(value)
    }
    const handleCreateSuccess = (value) => {
        setCreateSuccess(value)
    }

    useEffect(() => {
        fetching()
        setCreateSuccess(null)
    }, [after, createSuccess])


    return (
        <div className="w-4/5 mx-auto pt-10">
            <div className="flex justify-between items-center">
                <div>
                    <h1 className="text-2xl">Open Issues</h1>
                    <div className="mt-1">
                        <Link to={"/"} className="text-blue-400">Home </Link>
                        <Link to={`${path.REPOS}/${slug}`} className="text-blue-400">/ Repositories </Link>
                        <span className="text-gray-500">/ Open Issues</span>
                    </div>
                </div>
                <NewIssueModal 
                    id={repoId} 
                    handleCreateSuccess={handleCreateSuccess} 
                    createSuccess={createSuccess} 
                />
            </div>
            {issuesLoading ? (
                <div className="text-center mt-40"><p>Loading...</p></div>
            ): issues?.length > 0 ? (
                <div className="mt-10">
                    {
                        issues.map((issue, index) => (
                            <div className="flex justify-between mb-4" key={index}>
                                <h3 className="font-semibold">{issue.node.title}</h3>
                                <p>by {issue.node.author.login}</p>
                            </div>
                        ))
                    }
                    {
                        pageInfo && (
                            <Pagination 
                                hasPreviousPage={pageInfo.hasPreviousPage}
                                hasNextPage={pageInfo.hasNextPage}
                                handleAfter={handleAfter}
                                handleBefore={handleBefore}
                                startCursor={pageInfo.startCursor}
                                endCursor={pageInfo.endCursor}
                            />
                        )
                    }
                </div>
            ): <div className="text-center mt-20"><h1 className="text-lg">No issues found</h1></div>}
        </div>
    )
}

export default Index
