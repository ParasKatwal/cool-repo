
function Index(
    {hasPreviousPage, hasNextPage, handleAfter, handleBefore, startCursor, endCursor}
) {
    const onPrevious = () => {
        handleAfter(null)
        handleBefore(JSON.stringify(startCursor))
    }

    const onNext = () => {
        handleBefore(null)
        handleAfter(JSON.stringify(endCursor))
    }

    return (
        <div className="pt-4">
            <button 
                className={`bg-black text-white py-1 px-4 mr-4 ${hasPreviousPage ? '': 'opacity-70 cursor-not-allowed'}`} 
                disabled={!hasPreviousPage}
                onClick={() => onPrevious()}
            >
                Previous
            </button>
            <button 
                className={`bg-black text-white py-1 px-4 ${hasNextPage ? '': 'opacity-70 cursor-not-allowed'}`} 
                disabled={!hasNextPage}
                onClick={() => onNext()}
            >
                Next
            </button>
        </div>
    )
}

export default Index
