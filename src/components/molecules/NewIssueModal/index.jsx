import { useState } from 'react'
import Modal from 'react-modal';

import { API_BASE_URI } from '../../../constants/url';
import { ACCESS_TOKEN } from '../../../constants/url';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

Modal.setAppElement('#root');

function Index({id, handleCreateSuccess, createSuccess}) {
    const [modalIsOpen, setIsOpen] = useState(false);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [title, setTitle] = useState(null);
    const [description, setDescription] = useState(null);
    const repoId = JSON.stringify(id)

    const onTitleChange = (e) => {
        setTitle(JSON.stringify(e.target.value))
    }
    const onDescriptionChange = (e) => {
        setDescription(JSON.stringify(e.target.value))
    }

    function openModal() {
        setIsOpen(true);
    }

    function closeModal() {
        setIsOpen(false);
    }

    const handleSubmit = (e) => {
        console.log(description)
        e.preventDefault();
        setLoading(false)
        setError(null)
        fetching()
    }

    const LAUNCHES_QUERY =`
        mutation CreateIssue {
            createIssue(input: {repositoryId: ${repoId}, title: ${title}, body: ${description}}) {
                issue {
                    number
                }
            }
        }
    `

    const fetching = () => {
        setLoading(true)
        fetch(API_BASE_URI, {
            // mode: "no-cors",
            method: "POST",
            headers: { "Content-Type": "application/json", Authorization: "bearer "+ACCESS_TOKEN },
            body: JSON.stringify({ query: LAUNCHES_QUERY })
        }).then(response => response.json())
        .then(data => {
            handleCreateSuccess(true)
            setLoading(false)
            closeModal()
            console.log('created')
        } )
        .catch(error => {
            console.log(error) 
            setLoading(false)
        })
    }

    return (
        <>
            <button className="border border-black px-4 h-10" onClick={openModal}>New Issue</button>
            <Modal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                style={customStyles}
                contentLabel="Example Modal"
            >   
                <div className="w-96">
                    {createSuccess === false && (
                        <div className="bg-red-50 text-center py-2 mb-4">
                            <h1 className="text-red-500">Error! Please try again.</h1>
                        </div>
                    )}
                    <h1 className="font-semibold mb-6 text-lg">New Issue</h1>
                    <form className="flex flex-col" onSubmit={handleSubmit}>
                        <input 
                            className="outline-none border border-gray-300 px-2.5 py-2 mb-4" 
                            type="text" 
                            placeholder="Title" 
                            onChange={ e => onTitleChange(e)}
                        />
                        <textarea 
                            className="outline-none border border-gray-300 px-2.5 py-2" 
                            placeholder="Description"
                            cols="30" 
                            rows="10"
                            onChange={ e => onDescriptionChange(e)}></textarea>
                        <div className="mt-10 flex justify-end">
                            <button 
                                className={`border border-black px-6 py-1.5 mr-4 ${loading ? 'opacity-70 cursor-not-allowed' : ''}`} 
                                onClick={closeModal} 
                                disabled={loading}
                            >
                                Cancel
                            </button>
                            <button 
                                className={`bg-black text-white px-6 py-1.5 ${loading ? 'opacity-70 cursor-not-allowed' : ''}`}
                                disabled={loading}
                            >
                                {loading ? "Creating..." : "Create"}
                            </button>
                        </div>
                    </form>
                </div>
            </Modal>
        </>
    )
}

export default Index
