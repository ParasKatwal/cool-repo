import { Suspense } from 'react';
import RouterApp from './services/Router';
import {
  BrowserRouter as Router,
} from "react-router-dom";
import Fallback from './components/organisms/Fallback'

function App() {
  return (
    <Suspense fallback={<Fallback />}>
      <Router>
        <RouterApp />
      </Router>
    </Suspense>
  );
}

export default App;
